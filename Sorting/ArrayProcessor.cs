﻿
namespace Sorting
{
   public static class ArrayProcessor
   {
      public static int[] MergeAndReorder(int[] firstArray, int[] secondArray)
      {
         return Reverse(MergeAndSort(firstArray, secondArray));
      }

      public static int[] MergeAndSort(int[] firstArray, int[] secondArray)
      {
         var result = new int[firstArray.Length + secondArray.Length];
         int firstArrayCounter = 0, secondArrayCounter = 0, resultArrayCounter = 0;
         while (firstArrayCounter < firstArray.Length && secondArrayCounter < secondArray.Length)
         {
            if (firstArray[firstArrayCounter] < secondArray[secondArrayCounter])
            {
               result[resultArrayCounter] = firstArray[firstArrayCounter];
               firstArrayCounter++;
            }
            else
            {
               result[resultArrayCounter] = secondArray[secondArrayCounter];
               secondArrayCounter++;
            }
            resultArrayCounter++;
         }

         while (firstArrayCounter < firstArray.Length)
         {
            result[resultArrayCounter] = firstArray[firstArrayCounter];
            firstArrayCounter++;
            resultArrayCounter++;
         }

         while (secondArrayCounter < secondArray.Length)
         {
            result[resultArrayCounter] = secondArray[secondArrayCounter];
            secondArrayCounter++;
            resultArrayCounter++;
         }
         return result;
      }

      public static int[] Reverse(int[] array)
      {
         for (var i = 0; i < array.Length / 2; i++)
         {
            var currentValue = array[i];
            array[i] = array[array.Length - i - 1];
            array[array.Length - i - 1] = currentValue;
         }
         return array;
      }
   }
}
