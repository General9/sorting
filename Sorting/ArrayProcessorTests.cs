﻿using NUnit.Framework;

namespace Sorting
{
   [TestFixture]
   public class ArrayProcessorTests
   {
      [Test]
      public void MergeAndSortEmptyArrays()
      {
         var firstArray = new int[]{};
         var secondArray = new int[]{};
         var result = ArrayProcessor.MergeAndSort(firstArray, secondArray);
         Assert.AreEqual(0, result.Length);
      }

      [Test]
      public void MergeAndSortLeftEmptyArray()
      {
         var firstArray = new[]{1, 2, 4};
         var secondArray = new int[]{};
         var result = ArrayProcessor.MergeAndSort(firstArray, secondArray);
         Assert.AreEqual(3, result.Length);
         Assert.AreEqual(1, result[0]);
         Assert.AreEqual(2, result[1]);
         Assert.AreEqual(4, result[2]);
      }

      [Test]
      public void MergeAndSortRightEmptyArray()
      {
         var firstArray = new int[]{};
         var secondArray = new[] {1, 2, 4};
         var result = ArrayProcessor.MergeAndSort(firstArray, secondArray);
         Assert.AreEqual(3, result.Length);
         Assert.AreEqual(1, result[0]);
         Assert.AreEqual(2, result[1]);
         Assert.AreEqual(4, result[2]);
      }

      [Test]
      public void MergeAndSortArrays()
      {
         var firstArray = new[] { 1, 4, 8 };
         var secondArray = new[] { 1, 2, 4 };
         var result = ArrayProcessor.MergeAndSort(firstArray, secondArray);
         Assert.AreEqual(6, result.Length);
         Assert.AreEqual(1, result[0]);
         Assert.AreEqual(1, result[1]);
         Assert.AreEqual(2, result[2]);
         Assert.AreEqual(4, result[3]);
         Assert.AreEqual(4, result[4]);
         Assert.AreEqual(8, result[5]);
      }

      [Test]
      public void ReverseEmptyArray()
      {
         var firstArray = new int[] { };
         var result = ArrayProcessor.Reverse(firstArray);
         Assert.AreEqual(0, result.Length);
      }

      [Test]
      public void ReverseOneElementArray()
      {
         var firstArray = new[] {1};
         var result = ArrayProcessor.Reverse(firstArray);
         Assert.AreEqual(1, result.Length);
         Assert.AreEqual(1, result[0]);
      }

      [Test]
      public void ReverseArray()
      {
         var firstArray = new[] { 1, 4, 8 };
         var result = ArrayProcessor.Reverse(firstArray);
         Assert.AreEqual(3, result.Length);
         Assert.AreEqual(8, result[0]);
         Assert.AreEqual(4, result[1]);
         Assert.AreEqual(1, result[2]);
      }

      [Test]
      public void MergeAndReorderArray()
      {
         var firstArray = new[] { 1, 4, 8 };
         var secondArray = new[] { 1, 2, 4 };
         var result = ArrayProcessor.MergeAndReorder(firstArray, secondArray);
         Assert.AreEqual(6, result.Length);
         Assert.AreEqual(1, result[5]);
         Assert.AreEqual(1, result[4]);
         Assert.AreEqual(2, result[3]);
         Assert.AreEqual(4, result[2]);
         Assert.AreEqual(4, result[1]);
         Assert.AreEqual(8, result[0]);
      }
   }
}
